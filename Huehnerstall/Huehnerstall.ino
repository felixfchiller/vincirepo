/* Versionsverlauf
 *  ================================================================================================================================
 * *********
 * v005
 * *********
 * Änderungsdatum: 11.04.2021
 * Anpassung Uhrzeit: Klappe schließt frühestens 18:00 Uhr und spätestens 21:00 Uhr
 * 
 * *********
 * v006
 * *********
 * Änderungsdatum: 04.11.2021
 * Neuauflage ohne Funktion zur Lichtzeit. Licht ist einfach zwischen 6 und 22 Uhr an.
 * 
 * ==================================================================================================================================*/


//Bibliotheken
#include "RTClib.h"

// RTC Objekt erstellen
RTC_DS3231 rtc;

// Pin 0 und 1 sind für Bus für RTC
// Eingänge Definitionen und Variablendefinition
// Gleichstrommotor 1

uint8_t GSM1  = 6;
uint8_t m11   = 7;
uint8_t m12   = 8;

// Gleichstrommotor 2

//int GSM2  = 0;
//int m21   = 0;
//int m22   = 0;

//Endschalter
uint8_t endOut = 13;

uint8_t switchReedUpper  = 12;
uint8_t switchReedLower  = 11;

// taster 
/////////////////////////////////////!!!!!!!!!!!!!!!!!!!! gpios korrekt belegen
uint8_t switchKlappePin = 2;
uint8_t switchLichtPin = 3;

//Photodiode
int PhotoPin = A0;
int photoVal = 0;

//Relais für Beleuchtung
uint8_t R1 = 9;
uint8_t R2 = 10;
uint8_t R3 = 4;
uint8_t R4 = 5;

uint8_t LED1 = R3;
uint8_t LED2 = R4;
uint8_t LED3 = R1;
uint8_t LED4 = R2;

uint8_t stateKlappe = 0;
uint8_t stateLicht = 0;

uint8_t timeToTurnOff = 60;   //Sekunden bis motor abschaltet wenn Endlageschalter nicht gedrückt

// zeiten 
uint16_t y = 0;
uint8_t M = 0;
uint8_t d = 0;
uint8_t h = 0;
uint8_t m = 0;

uint16_t dayOfYear = 0;


// ALLES NACH SOMMERZEIT
//                                   01.Jan. , 01.Feb. , 01.Mär. , 01.Apr. , 01.Mai. , 01.Jun. , 01.Jul. , 01.Aug. , 01.Sep. , 01.Okt. , 01.Nov. , 01.Dez. , 01.Jan.
uint16_t sunriseTimes[13]         = { 9*60+14,  8*60+51,  8*60+03, 07*60+00, 06*60+02, 05*60+24, 05*60+24, 05*60+56, 06*60+39, 07*60+21,  8*60+ 8,  8*60+53,  9*60+14};
uint16_t sunsetTimes[13]          = {17*60+35, 18*60+18, 19*60+04, 19*60+51, 20*60+35, 21*60+14, 21*60+26, 20*60+57, 20*60+02, 18*60+59, 18*60+00, 17*60+26, 17*60+35};
uint16_t sunriseTimeToday = 0;
uint16_t sunsetTimeToday = 0;
uint16_t minutesToday = 0;

uint16_t doorCloseTime = 0;
uint16_t doorOpenTime = 0;
uint16_t lightOnTime = 05*60+45;    // 05:45 Uhr
uint16_t lightOffTime = 20*60+00;   // 21:00 Uhr
uint16_t lightOffToDoorClose = 0*60+10;    // Zeit von Licht aus bis Klappe zu: 10min (Geht danach wieder an)

// Handbetätigung
bool manualUp = false;
bool manualDown = false;

//Funktionen

void calcTimes()
{
    DateTime now = rtc.now();
  y = now.year();     // 2021: y=2021
  M = now.month();    // Januar: M=1
  d = now.day();      // 1.Tag des Monats: d=1
  m = now.minute();
  h = now.hour();
  
  minutesToday = 60*h + m;
  
  sunriseTimeToday = sunriseTimes[M-1] + (d-1)*((float)sunriseTimes[M]-(float)sunriseTimes[M-1])/30;
  sunsetTimeToday  = sunsetTimes[M-1] + (d-1)*((float)sunsetTimes[M]-(float)sunsetTimes[M-1])/30;

  doorOpenTime = sunriseTimeToday; // + 45
  doorCloseTime = sunsetTimeToday + 45;
}
void readPhotoVals()
{
  digitalWrite(endOut,HIGH);
  photoVal = analogRead(PhotoPin);
  digitalWrite(endOut,LOW);
}

void printPhotoVals()
{
  Serial.print("Photosensor: ");
  Serial.println(photoVal);
}

void printSwitchPos()
{
  digitalWrite(endOut,HIGH);
  
  if(!digitalRead(switchReedUpper))
    Serial.print("Schalter oben ist gedrückt");
  else
    Serial.print("Schalter oben ist nicht gedrückt");
    
  if(!digitalRead(switchReedLower))
    Serial.println(", Schalter unten ist gedrückt");
  else
    Serial.println(", Schalter unten ist nicht gedrückt");
    
  digitalWrite(endOut,LOW);
}

void DriveUp()
{
  digitalWrite(m11, LOW);  // Motor 1 beginnt zu rotieren    CW
  digitalWrite(m12, HIGH);
  analogWrite(GSM1, 255);   //evtl Unnötig, wird nicht abgeschaltet
  digitalWrite(endOut,HIGH);
  Serial.println("Klappe fährt hoch");
  int klappeCounter = 0;
  //startMillis = millis();
  //Serial.println(startMillis);
  while(true)
  {
    if(!digitalRead(switchReedUpper))    //Wenn Endschalter erreicht
    {
      digitalWrite(m11, LOW);  // Motor 1 stoppt
      digitalWrite(m12, LOW);
      digitalWrite(endOut,LOW);
      
      return;
    }
    klappeCounter++;
    delay(10);
    if(klappeCounter>6000){
      digitalWrite(m11, LOW);  // Motor 1 stoppt
      digitalWrite(m12, LOW);
      digitalWrite(endOut,LOW);
      Serial.println("Klappe hat mehr als 100 sek. zum Hochfahren benötigt");
      stateKlappe = 30;
    }
  }
}

void DriveDown()
{
  digitalWrite(m11, HIGH);  // Motor 1 beginnt zu rotieren     CCW
  digitalWrite(m12, LOW);
  analogWrite(GSM1, 255);   //evtl Unnötig, wird nicht abgeschaltet
  digitalWrite(endOut,HIGH);
  int klappeCounter = 0;
  while(true)
  {
    if(!digitalRead(switchReedLower))    //Wenn Endschalter erreicht
    {
      digitalWrite(m11, LOW);  // Motor 1 stoppt
      digitalWrite(m12, LOW);
      digitalWrite(endOut,LOW);
      
      return;
    }
    klappeCounter++;
    delay(10);
    if(klappeCounter>6000){
      digitalWrite(m11, LOW);  // Motor 1 stoppt
      digitalWrite(m12, LOW);
      digitalWrite(endOut,LOW);
      Serial.println("Klappe hat mehr als 100 sek. zum runterfahren benötigt");
      stateKlappe = 30;
    }
  }
}

void printClock()
{
    //Ausgabe der aktuellen Uhrzeit, kann man auskommentieren
  Serial.println("RTC: ");    //zB RTC: 7:45 Uhr
//  sprintf("Date: %02d.%02d.%04d\n",d,M,y);
//  sprintf("Time: %02d:%02d Uhr\n",h,m);
  
  Serial.print(d);
  Serial.print(".");
  Serial.print(M);
  Serial.print(".");
  Serial.println(y);
  
  Serial.println("SOMMERZEIT:");
  Serial.print(h);
  Serial.print(":");
  Serial.print(m);
  Serial.println(" Uhr");
  
  Serial.print("Klappe geht auf um ");
  Serial.print(doorOpenTime/60);
  Serial.print(":");
  Serial.println(doorOpenTime%60);
  Serial.print("Klappe geht zu um ");
  Serial.print(doorCloseTime/60);
  Serial.print(":");
  Serial.println(doorCloseTime%60);
  
  Serial.print("Licht geht an um ");
  Serial.print(lightOnTime/60);
  Serial.print(":");
  Serial.println(lightOnTime%60);
  Serial.print("Licht geht aus um ");
  Serial.print(lightOffTime/60);
  Serial.print(":");
  Serial.println(lightOffTime%60);
}

void switchKlappe()
{
    if(stateKlappe == 10)
    {
        manualDown = true;
        manualUp = false;
    }
    else if(stateKlappe == 20)
    {
        manualDown = false;
        manualUp = true;
    }
}

void switchLicht()
{
    if(digitalRead(LED3) == true)
    {
        digitalWrite(LED3, LOW);
        digitalWrite(LED3, LOW);
    }
    else
    {
        digitalWrite(LED3, HIGH);
        digitalWrite(LED3, HIGH);
    }
}

void setup()                                                                                            // SETUP
{
  pinMode(GSM1, OUTPUT);
  pinMode(m11, OUTPUT);
  pinMode(m12, OUTPUT);
  pinMode(endOut, OUTPUT);
  pinMode(PhotoPin, INPUT);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);

  digitalWrite(LED1, LOW);
  digitalWrite(LED2, LOW);

  pinMode(switchReedUpper, INPUT);
  pinMode(switchReedLower, INPUT); 
  pinMode(endOut, OUTPUT);  
  // pinMode(switchAT, INPUT_PULLUP);
  pinMode(switchKlappePin, INPUT_PULLUP);
  pinMode(switchLichtPin, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(switchKlappePin),switchKlappe, CHANGE);
  attachInterrupt(digitalPinToInterrupt(switchLichtPin),switchLicht, CHANGE);

  Serial.begin(9600);

  //RTC initialisieren
  //==================
  
  if (! rtc.begin()) 
  {
    Serial.print("Couldn't find RTC");
    Serial.flush();
    abort();
  }
  
  if (rtc.lostPower()) 
  {
    Serial.println("RTC lost power, you should set the time!");
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time this sketch was compiled
//    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }
  //rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  rtc.disable32K(); //32kHz Clock ausschalten. (Default Wert ist an) -> Spart Strom
}

void loop()                                                                                         // LOOP
{
  calcTimes();
  
  readPhotoVals();
  printPhotoVals();
  
  //printSwitchPos();
  printClock();

// automatikbetrieb ist immer an

  // klappenansteuerung
  switch (stateKlappe) 
    {
      case 0: // initialisieren und referenzieren
        digitalWrite(endOut,HIGH);
        Serial.println("case 0: initialisierung");
        if (not((!digitalRead(switchReedUpper)) or (!digitalRead(switchReedLower)))) // falls klappe weder auf noch zu ist wird automatisch in definierten zustand gebracht
           {  
              Serial.println("case 0: undefinierte position Klappe");
              Serial.println("case 0: Klappe fährt hoch für Referenzierung");
              DriveUp();
              Serial.println("case 0: klappe oben");
              if(stateKlappe != 30) stateKlappe = 10;
              Serial.println("springe in case 10");
           }
        else if (!digitalRead(switchReedUpper))
          {
              Serial.println("case 0: klappe bereits oben");
              if(stateKlappe != 30) stateKlappe = 10;
              Serial.println("springe in case 10");
          }
        else if (!digitalRead(switchReedLower))
          {
              Serial.println("case 0: klappe bereits unten");
              stateKlappe = 20;
              Serial.println("springe in case 20");
          }
        digitalWrite(endOut,LOW);
        break;
      case 10: // up
        Serial.println("case 10: klappe oben");
        if(minutesToday > doorOpenTime and minutesToday < doorCloseTime)
        {
           manualUp = false;
        } 
         //if (((h >= 18) and (photoVal <= 80)) or (h >= 21))
         if((minutesToday >= doorCloseTime or manualDown == true or manualDown == true) and (manualUp == false))
           {
              Serial.println("springe in case 21");
              stateKlappe = 21; 
           }           
        break;
        
      case 11: // drive up
        Serial.println("case 11: klappe fährt rauf");
        DriveUp();
        stateKlappe = 10;
        Serial.println("springe zu case 10");
        break;
        
      case 20: // down
          Serial.println("case 20: klappe unten");
           if(minutesToday > doorCloseTime)
           {
               manualDown = false;
           }        
          //if (h > 6 and h < 18)
          //if (photoVal >= 40 or h > 9)
          if((minutesToday >= doorOpenTime and minutesToday < doorCloseTime or manualUp == true) and (manualDown == false)) 
           {
              Serial.println("springe zu case 11 (hochfahren)");
              stateKlappe = 11; 
           }
        break;
        
      case 21: // drive down
        Serial.println("case 21: klappe fährt runter");
        DriveDown();
        stateKlappe = 20;
        Serial.println("springe zu case 20");
        break;
      case 30:  //ERROR
        Serial.println("case 30: Fataler Error. Klappe hat zu lange gebraucht. wahrscheinlich Schnur gerissen");
        
        break;
    }
    
  // lichtansteuerung
  switch (stateLicht) 
  {
    case 0: // init   (undefiniert)
      //if (h >= lightOnHour and h < lightOffHour) 
      //if((h >= 6) and (h < 22))
      if(minutesToday >= lightOnTime and minutesToday < lightOffTime)
         {
            Serial.println("springe zu licht an");
            digitalWrite(LED1, LOW);
            digitalWrite(LED2, LOW);
            stateLicht = 1;
         }
      else    //schalte licht aus
        {
            Serial.println("springe zu licht aus");
            digitalWrite(LED1, HIGH);
            digitalWrite(LED2, HIGH);
            stateLicht = 2;
        }
      break;
      
    case 1: // light is on
      Serial.println("case 1: licht ist an");        
      //if (h >= 22)
      if(minutesToday >= lightOffTime or (minutesToday >= doorCloseTime-lightOffToDoorClose and minutesToday < doorCloseTime+3)) 
        {
          Serial.println("springe zu licht aus");
          digitalWrite(LED1, HIGH);
          digitalWrite(LED2, HIGH);
          digitalWrite(LED3, HIGH);
          digitalWrite(LED4, HIGH);
          stateLicht = 2;
        }
       break; 
       
    case 2: // light is off
      Serial.println("case 2: licht ist aus");
      //if ((h >= 6) and (h <= lightOnHour + 1))
      if((minutesToday >= lightOnTime and minutesToday < doorCloseTime-lightOffToDoorClose) or (minutesToday >= doorCloseTime+3 and minutesToday < lightOffTime))
        {
          Serial.println("springe zu licht an");
          digitalWrite(LED1, LOW);
          digitalWrite(LED2, LOW);
          digitalWrite(LED3, LOW);
          digitalWrite(LED4, LOW);
          stateLicht = 1;
        }
       break; 
  }
  delay(10000);
}
